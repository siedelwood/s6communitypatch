--------------------------------------------------------------------------
--        *****************VILLAGERS AND CLOISTERS*****************
--------------------------------------------------------------------------

GUI_Merchant = {}

g_Merchant = {}

--for saving the current active merchant
g_Merchant.ActiveMerchantBuilding = 0

--consts to identify the trader inside a merchant building
g_Merchant.GoodTrader = 0
g_Merchant.MercenaryTrader = 1
g_Merchant.EntertainerTrader = 2

--for saving the tradertype and the offerindex for the buttons
g_Merchant.Offers = {}


function GameCallback_NPCReset()

    g_Merchant.ActiveMerchantBuilding = 0

end

function GameCallback_NPCInteraction(_PlayerID, _TargetEntityID)

    --GUI.AddNote("DEBUG: Active Merchant Building "..g_Merchant.ActiveMerchantBuilding)

    -- update GUI only for current player
    if GUI.GetPlayerID() ~= _PlayerID then
        return
    end

    -- activate GUI for traders
    if  _TargetEntityID ~= 0
    and Logic.EntityGetPlayer(_TargetEntityID) ~= GUI.GetPlayerID() -- not for own merchants
    and Logic.IsBuilding(_TargetEntityID) == 1                      -- only for buildings
    and Logic.GetNumberOfMerchants(_TargetEntityID) ~= 0            -- trader is in the building
    and _TargetEntityID ~= g_Merchant.ActiveMerchantBuilding        -- it is not activated yet
    then
        -- select merchant that is on screen
        local X, Y = GUI.GetEntityInfoScreenPosition(_TargetEntityID)
        local ScreenX, ScreenY = XGUIEng.GetWidgetScreenSize("/InGame/Root")

        if X > 50 and Y > 50 and X < (ScreenX - 50) and Y < (ScreenY - 50) then
            -- save merchant building
            g_Merchant.ActiveMerchantBuilding = _TargetEntityID

            -- update once before buttons are enabled, because buttons disable themself, if offer table is empty
            GUI_Merchant.ButtonsUpdater()

            -- show Widgets
            XGUIEng.ShowWidget("/InGame/Root/Normal/Selected_Merchant", 1)
            XGUIEng.ShowAllSubWidgets("/InGame/Root/Normal/Selected_Merchant/MerchantOffers", 1)
            return
        end
    end

    -- callback for interactive object interaction
    if Logic.IsInteractiveObject(_TargetEntityID) then
        ScriptCallback_ObjectInteraction(_PlayerID, _TargetEntityID)
    end

    -- callback for NPC interaction
    if MapScriptNPCInteraction ~= nil then
        MapScriptNPCInteraction (_TargetEntityID)
    end

end

----------------------------------------------------------------------------------------------------------------
-- Close the UI
----------------------------------------------------------------------------------------------------------------
function GameCallback_CloseNPCInteraction(_Player,_TargetEntityID)

    if _Player ~= GUI.GetPlayerID() then

        return

    end

    if g_Merchant.ActiveMerchantBuilding == _TargetEntityID then

        -- Disable UI
        XGUIEng.ShowWidget("/InGame/Root/Normal/Selected_Merchant", 0)

        -- Forget current merchant
        g_Merchant.ActiveMerchantBuilding = 0
        return
    end

    -- Callback for Interactive Object interaction
    if Logic.IsInteractiveObject(_TargetEntityID) then
        ScriptCallback_CloseObjectInteraction(_Player, _TargetEntityID)
    end

end

----------------------------------------------------------------------------------------------------------------
-- Position buttons and map offers to buttons in a global table
----------------------------------------------------------------------------------------------------------------
function GUI_Merchant.ButtonsUpdater()

    local BuildingID = g_Merchant.ActiveMerchantBuilding
    local PlayerID   = GUI.GetPlayerID()

    -- No merchant building is active, or destroyed
    ---------- do not show the buttons anymore, when trader has been closed
    if BuildingID == 0
    or Logic.IsEntityDestroyed(BuildingID) == true
    or Logic.GetTraderPlayerState(BuildingID, PlayerID) == 2 then
        GameCallback_CloseNPCInteraction(PlayerID, BuildingID)
        return
    end

    --------- Update the position
    local x,y = GUI.GetEntityInfoScreenPosition(BuildingID)
    if x ~= 0 and y ~= 0 then
        XGUIEng.ShowWidget("/InGame/Root/Normal/Selected_Merchant/MerchantOffers", 1)
        local WidgetSize = {XGUIEng.GetWidgetScreenSize("/InGame/Root/Normal/Selected_Merchant")}
        XGUIEng.SetWidgetScreenPosition("/InGame/Root/Normal/Selected_Merchant",x - (WidgetSize[1]/2),y + (WidgetSize[2]/2))
    else
        XGUIEng.ShowWidget("/InGame/Root/Normal/Selected_Merchant/MerchantOffers", 0)
    end

    --------- Update the button mapping
    -- Count the offers in all traders
    local AmountOfOffers = 0

    -- Go through all traders in the building
    local TradersInTheActiveBuilding = Logic.GetNumberOfMerchants(BuildingID)

    for i = 0, TradersInTheActiveBuilding - 1 do

        -- Get the trader type
        local TraderType

        if Logic.IsGoodTrader(BuildingID, i) then

            TraderType = g_Merchant.GoodTrader

        elseif Logic.IsMercenaryTrader(BuildingID, i) then

            TraderType = g_Merchant.MercenaryTrader

        elseif Logic.IsEntertainerTrader(BuildingID,i) then

            TraderType = g_Merchant.EntertainerTrader

        else

            GUI.AddNote("DEBUG: This trader type is not known!!")

        end

        -- Go through all offers of the trader in this loop
        local Offers = {Logic.GetMerchantOfferIDs(BuildingID, i,PlayerID)}

        for j = 1, #Offers do

            local ButtonIndex = AmountOfOffers + 1
            local OfferIndex  = Offers[j]

            -- Save the merchant type and the offerindex of this merchant, so the button can get this information
            g_Merchant.Offers[ButtonIndex]= {}
            g_Merchant.Offers[ButtonIndex].TraderType   = TraderType
            g_Merchant.Offers[ButtonIndex].OfferIndex   = OfferIndex

            -- Increase amount of offers
            AmountOfOffers = AmountOfOffers + 1

            -- There are only 4 buttons for all traders and their offers
            if AmountOfOffers > 4 then
                if Debug_EnableDebugOutput then
                
                    GUI.AddNote("DEBUG: To much offers in this building!")
                    
                end
            end

        end

    end

    -- Clear table if less than 4 offers are in the merchant building
    if AmountOfOffers < 4 then

        for k = AmountOfOffers, 4 do

            local ButtonIndex = k + 1

            g_Merchant.Offers[ButtonIndex] = nil

        end

    end

end

----------------------------------------------------------------------------------------------------------------
--  Button updates his texture, the amount or delets itself
----------------------------------------------------------------------------------------------------------------
function GUI_Merchant.OfferUpdate( _ButtonIndex )

    local CurrentWidgetID   = XGUIEng.GetCurrentWidgetID()
    local CurrentWidgetMotherID = XGUIEng.GetWidgetsMotherID(CurrentWidgetID)

    local PlayerID          = GUI.GetPlayerID()
    local BuildingID        = g_Merchant.ActiveMerchantBuilding

    -- No merchant building is active, or destroyed
    if BuildingID == 0
    or Logic.IsEntityDestroyed(BuildingID) == true then
        return
    end

    -- Do not show button if table does not exist
    if g_Merchant.Offers[_ButtonIndex] == nil then
        XGUIEng.ShowWidget(CurrentWidgetMotherID,0)
        return
    end

    -- Get offer data
    local TraderType        = g_Merchant.Offers[_ButtonIndex].TraderType
    local OfferIndex        = g_Merchant.Offers[_ButtonIndex].OfferIndex

    local GoodType, OfferGoodAmount, OfferAmount, AmountPrices = 0,0,0,0

    -- Check tradertype and update texture / text and amount of offers
    if TraderType == g_Merchant.GoodTrader then

        -- Get offer data
        GoodType, OfferGoodAmount, OfferAmount, AmountPrices = Logic.GetGoodTraderOffer(BuildingID,OfferIndex,PlayerID)

        --mr. merry hack
        if GoodType == Goods.G_Sheep
        or GoodType == Goods.G_Cow then
            OfferGoodAmount = 5
        end

        -- Transfer Texture
        SetIcon(CurrentWidgetID, g_TexturePositions.Goods[GoodType])

    elseif TraderType == g_Merchant.MercenaryTrader then

        -- Get offer data
        GoodType, OfferGoodAmount, OfferAmount, AmountPrices = Logic.GetMercenaryOffer(BuildingID,OfferIndex,PlayerID)

        -- Transfer Texture
        SetIcon(CurrentWidgetID, g_TexturePositions.Entities[GoodType])

    elseif TraderType == g_Merchant.EntertainerTrader then

        --Entertainer Type, actually
        GoodType, OfferGoodAmount, OfferAmount, AmountPrices = Logic.GetEntertainerTraderOffer(BuildingID,OfferIndex,PlayerID)

        -- Get offer data / mmh
        if not (Logic.CanHireEntertainer(PlayerID) == true
        and Logic.EntertainerIsOnTheMap(GoodType) == false) then
            OfferAmount = 0
        end

        -- Transfer texture
        SetIcon(CurrentWidgetID, g_TexturePositions.Entities[GoodType])
    end

    local OfferAmountWidget = XGUIEng.GetWidgetPathByID(CurrentWidgetMotherID) .. "/OfferAmount"
    XGUIEng.SetText(OfferAmountWidget, "{center}" .. OfferAmount)

    local OfferGoodAmountWidget = XGUIEng.GetWidgetPathByID(CurrentWidgetMotherID) .. "/OfferGoodAmount"
    XGUIEng.SetText(OfferGoodAmountWidget, "{center}" .. OfferGoodAmount)

    -- Check amount and update amount
    if OfferAmount == 0 then
        XGUIEng.DisableButton(CurrentWidgetID,1)
        --XGUIEng.SetText(OfferGoodAmountWidget, "{center}-")
    else
        XGUIEng.DisableButton(CurrentWidgetID,0)
    end

    --local OfferProgressWidget = XGUIEng.GetWidgetPathByID(CurrentWidgetMotherID) .. "/OfferProgress"
    --local OfferProgress = 5 --insert function here
    --XGUIEng.SetProgressBarValues(OfferProgressWidget, OfferProgress, 10)
end

----------------------------------------------------------------------------------------------------------------
-- Mouse over to set text and costs
----------------------------------------------------------------------------------------------------------------
function GUI_Merchant.OfferMouseOver( _ButtonIndex )

    if not g_Merchant.Offers[_ButtonIndex] then
        -- Like this should happen
        return
    end

    local CurrentWidgetID = XGUIEng.GetCurrentWidgetID()

    -- Get logic data
    local PlayerID          = GUI.GetPlayerID()
    local BuildingID        = g_Merchant.ActiveMerchantBuilding

    -- No merchant building is active
    if BuildingID == 0 then
        return
    end

    -- Get offer data
    local TraderType        = g_Merchant.Offers[_ButtonIndex].TraderType
    local OfferIndex        = g_Merchant.Offers[_ButtonIndex].OfferIndex

    -- Display costs
    local Price = ComputePrice( BuildingID, OfferIndex, PlayerID, TraderType )
    local Costs = {Goods.G_Gold, Price}

    -- Display text
    local TooltipTextKey

    if TraderType == g_Merchant.GoodTrader then
        local GoodType, OfferGoodAmount, Amount, AmountPrices = Logic.GetGoodTraderOffer(BuildingID, OfferIndex, PlayerID)
        TooltipTextKey = Logic.GetGoodTypeName(GoodType)
    elseif TraderType == g_Merchant.MercenaryTrader then
        TooltipTextKey = "HireMercenaries"
    elseif TraderType == g_Merchant.EntertainerTrader then
        TooltipTextKey = "HireEntertainer"
    end

    local CurrentWidgetMotherID = XGUIEng.GetWidgetsMotherID(CurrentWidgetID)

    if XGUIEng.IsButtonDisabled(CurrentWidgetID) == 1 then
        GUI_Tooltip.TooltipBuy(Costs, TooltipTextKey, "MerchantNoOffers", nil, nil, CurrentWidgetMotherID)
    else
        GUI_Tooltip.TooltipBuy(Costs, TooltipTextKey, nil, nil, nil, CurrentWidgetMotherID)
    end
end

----------------------------------------------------------------------------------------------------------------
-- Buy offer if possible
----------------------------------------------------------------------------------------------------------------
do
local BuyLock = {Locked = false}

function GameCallback_MerchantInteraction( _BuildingID, _PlayerID, _OfferID )
    if _PlayerID == GUI.GetPlayerID() then
        -- Ensure the player can only buy something after the event was processed
        BuyLock.Locked = false
    end
end

function GUI_Merchant.OfferClicked( _ButtonIndex )

    local CurrentWidgetID = XGUIEng.GetCurrentWidgetID()

    -- Get logic data
    local PlayerID              = GUI.GetPlayerID()
    local BuildingID            = g_Merchant.ActiveMerchantBuilding

    if BuildingID == 0 or BuyLock.Locked then
        return
    end

    local PlayersMarketPlaceID  = Logic.GetMarketplace(PlayerID)
    local TraderPlayerID        = Logic.EntityGetPlayer(BuildingID)

    -- Get offer data
    local TraderType        = g_Merchant.Offers[_ButtonIndex].TraderType
    local OfferIndex        = g_Merchant.Offers[_ButtonIndex].OfferIndex

    local GoodType, OfferGoodAmount, OfferAmount, AmountPrices = 0,0,0,0

    -- Check if offer can be bought
    local CanBeBought = true

    if TraderType == g_Merchant.GoodTrader then

        GoodType, OfferGoodAmount, OfferAmount, AmountPrices = Logic.GetGoodTraderOffer(BuildingID,OfferIndex,PlayerID)

        -- Is space in the storehouse?
        if Logic.GetGoodCategoryForGoodType(GoodType) == GoodCategories.GC_Resource then

            --ToDo: I think this has to be replaced by a function that gets the overall limit of a storehouse, NOT per goodtype.
            --local AmountInCastle = GetPlayerGoodsInSettlement( GoodType, PlayerID)
            --local LimitInCastle  = GetPlayerResourcesLimit( GoodType, PlayerID)

            local SpaceForNewGoods =  Logic.GetPlayerUnreservedStorehouseSpace(PlayerID)

            if SpaceForNewGoods < OfferGoodAmount then

                CanBeBought = false

                local MessageText = XGUIEng.GetStringTableText("Feedback_TextLines/TextLine_MerchantStorehouseSpace")
                Message(MessageText)

            end

        -- Animal can always be bought
        elseif Logic.GetGoodCategoryForGoodType(GoodType) == GoodCategories.GC_Animal then

            CanBeBought = true

        -- Goods need space on the marketplace and an upgraded marketplace / EnableRights is used, so this rule can be disabled by ALT F12 for testing
        else

            if Logic.CanFitAnotherMerchantOnMarketplace(PlayersMarketPlaceID) == false then

                CanBeBought = false

                local MessageText = XGUIEng.GetStringTableText("Feedback_TextLines/TextLine_MerchantMarketplaceFull")
                Message(MessageText)

            end

        end

    -- Entertainers needs an upgraded marketplace and space on it (same rule like good traders. Can this be merged?)
    elseif TraderType == g_Merchant.EntertainerTrader then

        if Logic.CanFitAnotherEntertainerOnMarketplace(PlayersMarketPlaceID) == false then

            CanBeBought = false
            local MessageText = XGUIEng.GetStringTableText("Feedback_TextLines/TextLine_MerchantMarketplaceFull")
            Message(MessageText)
        end

    elseif TraderType == g_Merchant.MercenaryTrader then

        GoodType, OfferGoodAmount, OfferAmount, AmountPrices = Logic.GetMercenaryOffer(BuildingID,OfferIndex,PlayerID)

        local CurrentSoldierCount = Logic.GetCurrentSoldierCount(PlayerID)
        local CurrentSoldierLimit = Logic.GetCurrentSoldierLimit(PlayerID)

        local SoldierSize
        if GoodType == Entities.U_Thief then
            SoldierSize = 1
        else
            SoldierSize = OfferGoodAmount
        end

        if (CurrentSoldierCount + SoldierSize) > CurrentSoldierLimit then
            CanBeBought = false
            local MessageText = XGUIEng.GetStringTableText("Feedback_TextLines/TextLine_SoldierLimitReached")
            Message(MessageText)
        end
    end

    -- Offer can be bought
    if CanBeBought == true then

        -- Get Price and Goldamount of player
        local Price = ComputePrice( BuildingID, OfferIndex, PlayerID, TraderType )
        local GoldAmountInCastle = GetPlayerGoodsInSettlement(Goods.G_Gold, PlayerID )

        --check if Merchant Cart can reach player (check for player's Gold Cart is superfluous)
        local PlayerSectorType = PlayerSectorTypes.Civil

        local IsReachable = CanEntityReachTarget(PlayerID, Logic.GetStoreHouse(TraderPlayerID), Logic.GetStoreHouse(PlayerID),
            nil, PlayerSectorType)

        if IsReachable == false then
            local MessageText = XGUIEng.GetStringTableText("Feedback_TextLines/TextLine_GenericUnreachable")
            Message(MessageText)
            return
        end

        -- Player has the gold
        if Price <= GoldAmountInCastle then

            -- Spawn animals at the building via script. Maybe we will move this into an own tradertype after alpha
            if Logic.GetGoodCategoryForGoodType(GoodType) == GoodCategories.GC_Animal then

                local AnimalType = Entities.A_X_Sheep01

                if GoodType == Goods.G_Cow then

                    AnimalType = Entities.A_X_Cow01

                end

                for i=1,5 do
                    GUI.CreateEntityAtBuilding(BuildingID, AnimalType, 0)
                end

            end

            BuyLock.Locked = true
            
            -- Set price
            GUI.ChangeMerchantOffer(BuildingID,PlayerID,OfferIndex,Price)

            -- Buy offer
            GUI.BuyMerchantOffer(BuildingID,PlayerID,OfferIndex)

            Sound.FXPlay2DSound( "ui\\menu_click")

            StartKnightVoiceForPermanentSpecialAbility(Entities.U_KnightTrading)
            StartKnightVoiceForPermanentSpecialAbility(Entities.U_KnightSabatta)
        else
            local MessageText = XGUIEng.GetStringTableText("Feedback_TextLines/TextLine_NotEnough_G_Gold")
            Message(MessageText)
        end
    end
end
end
----------------------------------------------------------------------------------------------------------------
-- Compute price
----------------------------------------------------------------------------------------------------------------
function ComputePrice(BuildingID,OfferID,PlayerID,TraderType)

    local TraderPlayerID = Logic.EntityGetPlayer(BuildingID)

    local Type = Logic.GetGoodOfOffer(BuildingID, OfferID, PlayerID, TraderType)

    local BasePrice = MerchantSystem.BasePrices[Type]

    if BasePrice == nil then
        GUI.AddNote("DEBUG: Price is not in table")
        return 3
    end

    -- add ability of knight trading
    local TraderAbility = Logic.GetKnightTraderAbilityModifier(PlayerID)
    BasePrice = math.ceil(BasePrice / TraderAbility)

    local OfferCount = Logic.GetOfferCount(BuildingID, OfferID, PlayerID, TraderType)

    -- clamp max price increase
    if OfferCount > 8 then
        OfferCount = 8
    end

    local Modifier = math.ceil(BasePrice / 4)

    local Result = BasePrice + (Modifier * OfferCount)

    if Result < BasePrice then
        Result = BasePrice
    end

    return Result

end

----------------------------------------------------------------------------------------------------------------
-- Send Back Merchant
----------------------------------------------------------------------------------------------------------------
function GUI_Merchant.SendBackClicked()
    Sound.FXPlay2DSound("ui\\menu_click")

    local EntityID = GUI.GetSelectedEntity()
    GUI.CommandMerchantToLeaveMarketplace(EntityID)
end
