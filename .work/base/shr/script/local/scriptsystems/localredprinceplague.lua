-- -------------------------------------------------------------------------- --
--                              RED PRINCE PLAGUE                             --
-- -------------------------------------------------------------------------- --

-- This script attempts to emulate the plague ability of the red prince. If this
-- will ever used in History Edition Multiplayer it will not work, cause Script 
-- Commands are blocked there. (Thank you, Ubisoft!!!)

-- (Called by lua code) Ability is started
function GameCallback_Feedback_OnRedPrinceAbilityStarted(_EntityID, _PlayerID)
    GUI.SendScriptCommand(string.format(
        "GameCallback_OnRedPrinceAbilityStarted(%d, %d)",
        _EntityID,
        _PlayerID
    ))
end

-- (Called by lua code) Ability can be activated
function GameCallback_Feedback_IsRedPrincePlaguePossible(_KnightID)
    return RedPrincePlague:IsActivationPossible(_KnightID);
end

-- -------------------------------------------------------------------------- --

RedPrincePlague = {
    Global = {
        PlayerInfected = {},
    },
}

function RedPrincePlague:Init()
    StartSimpleJob("RedPrincePlague_InformPlayerAboutPlague")
end

-- Check ability possible
function RedPrincePlague:IsActivationPossible(_KnightID)
    local PlayerID = Logic.EntityGetPlayer(_KnightID);
    local TerritoryID = GetTerritoryUnderEntity(_KnightID);
    local RivalPlayerID = Logic.GetTerritoryPlayerID(TerritoryID);
    
    -- Can not selfinfect
    if PlayerID == RivalPlayerID
    -- Knight is Red Prince
    or Logic.GetEntityType(_KnightID) ~= Entities.U_KnightRedPrince
    -- Can only infect unknown and enemies
    or (Diplomacy_GetRelationBetween(PlayerID, RivalPlayerID) >= 1
    -- Can not infect already infected players
    or self:IsPlayerInfected(RivalPlayerID)) then
        return false;
    end
    return true;
end

function RedPrincePlague:IsPlayerInfected(_PlayerID)
    return self.Global.PlayerInfected[_PlayerID] == true;
end

function RedPrincePlague_InformPlayerAboutPlague()
    local PlayerID = GUI.GetPlayerID();
    local KnightID = Logic.GetKnightID(PlayerID);
    if KnightID ~= 0 and RedPrincePlague:IsActivationPossible(KnightID) then
        StartKnightVoiceForActionSpecialAbility(Entities.U_KnightRedPrince);
    end
end