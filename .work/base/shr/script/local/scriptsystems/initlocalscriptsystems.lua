--------------------------------------------------------------------------
--        ***************** SCRIPT SYSTEMS (LOCAL) *****************
--------------------------------------------------------------------------

Script.Load( "Script\\Local\\ScriptSystems\\LocalVictoryCondition.lua" )
Script.Load( "Script\\Local\\ScriptSystems\\LocalMusicSystem.lua" )
Script.Load( "Script\\Local\\ScriptSystems\\LocalVideos.lua" )
Script.Load( "Script\\Shared\\ScriptSystems\\SharedConstants.lua" )
Script.Load( "Script\\Shared\\ScriptSystems\\SharedEndStatistic.lua" )
Script.Load( "Script\\Shared\\ScriptSystems\\SharedKnightTitleSystem.lua" )

-- Emulate red prince plague
Script.Load( "Script\\Local\\ScriptSystems\\LocalRedPrincePlague.lua" )


function InitLocalTables()

    -- for the Taxation Level for the moment 
    PlayerTaxationLevel = Logic.CreateReferenceToTableInGlobaLuaState("PlayerTaxationLevel")
    
    -- table for the calculation of the gold amount a building is paying
    TaxationGoldAmount = Logic.CreateReferenceToTableInGlobaLuaState("TaxationGoldAmount")
    
    -- current active needs of the players
    PlayerActiveNeeds  = Logic.CreateReferenceToTableInGlobaLuaState("PlayerActiveNeeds")
    
    PlayerSoldierPaymentLevel = Logic.CreateReferenceToTableInGlobaLuaState("PlayerSoldierPaymentLevel")
    
    SoldierPay = Logic.CreateReferenceToTableInGlobaLuaState("SoldierPay")
    
    --for the GetRelationBetween function
    DiplomaticEntity = Logic.CreateReferenceToTableInGlobaLuaState("DiplomaticEntity")
    DiplomaticEntities = Logic.CreateReferenceToTableInGlobaLuaState("DiplomaticEntities")
    
    --for the quests
    Quests = Logic.CreateReferenceToTableInGlobaLuaState("Quests")
    
    LockedKnightTitles = Logic.CreateReferenceToTableInGlobaLuaState("LockedKnightTitles")
    
    --for the knight titles    
    InitKnightTitleTables()
    
    
    MissionCounter = Logic.CreateReferenceToTableInGlobaLuaState("MissionCounter")
    
    g_EndStatistic = Logic.CreateReferenceToTableInGlobaLuaState("g_EndStatistic")
    

end