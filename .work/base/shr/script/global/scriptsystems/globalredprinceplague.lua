-- -------------------------------------------------------------------------- --
--                              RED PRINCE PLAGUE                             --
-- -------------------------------------------------------------------------- --

-- This script attempts to emulate the plague ability of the red prince. If this
-- will ever used in History Edition Multiplayer it will not work, cause Script 
-- Commands are blocked there. (Thank you, Ubisoft!!!)
--
-- How it works:
-- David Carridine aka Red Prince enters a territory of a player that must be
-- at war with him. Then he can infect settlers in an area of 1500 around him
-- with the plage. He always infects 3 settlers plus 1 more for each 10th
-- settler of the target player. This many settlers must be around though.
-- Then a plague strikes the target player for 7 Minutes. This plage spreads
-- like the Corona virus, meaning settlers have a chance to infect others when
-- they come to close. While one plague strikes a player, no other (Red Prince)
-- plague can be inflicted to this player.
-- 
-- Pharmacists are immune to the plague so that an player at least has a chance
-- to make medicine.
-- TODO: Is there a way to make a single settler sick without hitting the full
-- building and can a single settler somehow be cured by script?

-- (Called by lua code) Ability is started
function GameCallback_OnRedPrinceAbilityStarted(_EntityID, _PlayerID)
    Logic.DEBUG_AddNote("DEBUG: Player " .._PlayerID.. " started Red Prince ability!");
    
    -- Discharge ability
    Logic.KnightSetAbilityChargeSeconds(_EntityID, Abilities.AbilityConvert, 0);
    -- Trigger ability
    RedPrincePlague:StartPandemic(_EntityID, _PlayerID);
end

-- -------------------------------------------------------------------------- --

RedPrincePlague = {
    Infections     = {},
    PlayerInfected = {},

    Duration       = 7*60,
    InfectionRange = 1500,
    SpreadRange    = 900,
    SpreadChance   = 0.005,
    MinInfection   = 3,
}

function RedPrincePlague:Init()
    StartSimpleJob("RedPrincePlague_ControllerJob");
end

-- Starts a pandemic for the controler of the territory.
function RedPrincePlague:StartPandemic(_KnightID, _PlayerID)
    local TerritoryID = GetTerritoryUnderEntity(_KnightID);
    local RivalPlayerID = Logic.GetTerritoryPlayerID(TerritoryID);

    -- Not possible
    if not self:IsActivationPossible(_KnightID) then
        return;
    end
    -- No double infection
    if self:IsPlayerInfected(RivalPlayerID) then
        return;
    end
    -- Register as infected
    self:RegisterPlayerInfected(RivalPlayerID, true);

    -- Initial infection
    local PlagueVictims = {};
    local InfectedCount = 0;
    for k, v in pairs(GetPlayerEntities(RivalPlayerID, 0)) do
        if InfectedCount >= self.MinInfection + Logic.GetNumberOfEmployedWorkers(RivalPlayerID) / 10 then
            break;
        end
        if IsExisting(v) and (Logic.IsWorker(v) == 1 or Logic.IsSpouse(v) == true) then
            if Logic.GetEntityType(v) ~= Entities.U_Pharmacist then
                if not Logic.IsIll(v) and IsNear(v, _KnightID, self.InfectionRange) then
                    InfectedCount = InfectedCount +1;
                    Logic.MakeSettlerIll(v);
                    table.insert(PlagueVictims, v);
                end
            end
        end
    end

    -- Build data table
    table.insert(self.Infections, {
        SourcePlayer     = _PlayerID,
        TargetPlayer     = RivalPlayerID,
        Duration         = self.Duration,
        StartTime        = Logic.GetTime(),
        InfectedSettlers = PlagueVictims,
    });
end

-- Check ability possible
function RedPrincePlague:IsActivationPossible(_KnightID)
    local PlayerID = Logic.EntityGetPlayer(_KnightID);
    local TerritoryID = GetTerritoryUnderEntity(_KnightID);
    local RivalPlayerID = Logic.GetTerritoryPlayerID(TerritoryID);
    
    -- Can not selfinfect
    if PlayerID == RivalPlayerID
    -- Knight is Red Prince
    or Logic.GetEntityType(_KnightID) ~= Entities.U_KnightRedPrince
    -- Can only infect unknown and enemies
    or (Diplomacy_GetRelationBetween(PlayerID, RivalPlayerID) >= 1
    -- Can not infect already infected players
    or self:IsPlayerInfected(RivalPlayerID)) then
        return false;
    end
    return true;
end

-- Checks if player is already infected
function RedPrincePlague:IsPlayerInfected(_PlayerID)
    return self.PlayerInfected[_PlayerID] == true;
end

-- Controls the spread of the infection for all active plagues.
function RedPrincePlague:PandemicController()
    for i= #self.Infections, 1, -1 do
        if (Logic.GetTime() < self.Infections[i].StartTime + self.Infections[i].Duration)
        and #self.Infections[i].InfectedSettlers > 0 then
            for j= #self.Infections[i].InfectedSettlers, 1, -1 do
                if self:SuperSpreaderController(i, self.Infections[i].InfectedSettlers[j]) then
                    table.remove(self.Infections[i].InfectedSettlers, j);
                end
            end
        else
            -- Stop pandemic
            Logic.DEBUG_AddNote("DEBUG: Pandemic over for player " ..self.Infections[i].TargetPlayer);
            self:RegisterPlayerInfected(self.Infections[i].TargetPlayer, false);
            table.remove(self.Infections, i);
        end
    end
end

-- Controls the single infected settler and infection of close others.
function RedPrincePlague:SuperSpreaderController(_Index, _SuperSpreaderID)
    local TargetPlayerID = self.Infections[_Index].TargetPlayer;
    local x, y, z = Logic.EntityGetPos(_SuperSpreaderID);
    local EntitiesClose = {Logic.GetPlayerEntitiesInArea(TargetPlayerID, 0, x, y, self.SpreadRange, 48)};
    for i= 2, EntitiesClose[1] +1, 1 do
        if EntitiesClose[i] ~= _SuperSpreaderID and (Logic.IsWorker(EntitiesClose[i]) == 1 or Logic.IsSpouse(EntitiesClose[i]) == true) then
            if Logic.GetEntityType(EntitiesClose[i]) ~= Entities.U_Pharmacist then
                if not Logic.IsIll(EntitiesClose[i]) and IsNear(EntitiesClose[i], _SuperSpreaderID, self.InfectionRange) then
                    local InfectionChance = math.random();
                    if InfectionChance <= self.SpreadChance then
                        Logic.DEBUG_AddNote("DEBUG: Spreader entity " .._SuperSpreaderID.. " infects " ..EntitiesClose[i].. ".");
                        Logic.MakeSettlerIll(EntitiesClose[i]);
                        table.insert(self.Infections[_Index].InfectedSettlers, EntitiesClose[i]);
                    end
                end
            end
        end
    end
    return false;
end

-- Registers a player as infected.
function RedPrincePlague:RegisterPlayerInfected(_PlayerID, _Flag)
    Logic.ExecuteInLuaLocalState(string.format(
        "RedPrincePlague.Global.PlayerInfected[%d] = %s",
        _PlayerID,
        tostring(_Flag == true)
    ));
    self.PlayerInfected[_PlayerID] = _Flag == true;
end

function RedPrincePlague_ControllerJob()
    RedPrincePlague:PandemicController();
end