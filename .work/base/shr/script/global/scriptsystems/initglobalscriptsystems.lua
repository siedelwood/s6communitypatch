--------------------------------------------------------------------------
--        ***************** SCRIPT SYSTEMS (GLOBAL) *****************
--------------------------------------------------------------------------

Script.Load( "Script\\Shared\\ScriptSystems\\SharedConstants.lua" )
Script.Load( "Script\\Shared\\ScriptSystems\\SharedEndStatistic.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalInteractiveObjects.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalGameCallBacks.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalQuestSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalMerchantSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalNeedsSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalTaxSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalCityDevelopmentSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalCitySatisfactionSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalVictoryCondition.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalAssaultTemplate.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalDeliveryTemplate.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalDiplomacy.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalNPCDialog.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\Events\\EventSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalPath.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalShipPath.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalOutlawsSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalThiefSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalTreasureSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalFreeSettleModeSystem.lua" )
Script.Load( "Script\\Global\\ScriptSystems\\GlobalEndStatisticSystem.lua" )

-- Emulate red prince plague
Script.Load( "Script\\Global\\ScriptSystems\\GlobalRedPrincePlague.lua" )

function GUI_Message(_MessageKey, _playerID)
	if Logic.PlayerGetIsHumanFlag(_playerID) then
		Logic.ExecuteInLuaLocalState("LocalScriptCallback_StartVoiceMessage(" .. _playerID ..",'".. _MessageKey .."',true)" )
	end
end

function GUI_Note(_text)
	Logic.ExecuteInLuaLocalState("GUI.AddNote('".._text.."')")
end
