
function InitPlayersTrade()

    g_Trade = {}
    g_Trade.SellToPlayers = {}
    g_Trade.GoodType = 0
    g_Trade.GoodAmount = 0
    g_Trade.TargetPlayers = {}
    g_Trade.ShowGoodsInMultiTabOverride = true
    MerchantSystem = Logic.CreateReferenceToTableInGlobaLuaState("MerchantSystem")

    XGUIEng.ShowAllSubWidgets("/InGame/Root/Normal/AlignBottomRight/Selection", 0)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/SalesDialog",0)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/DialogButtons/PlayerButtons",0)

    XGUIEng.ShowAllSubWidgets("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons", 0)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/StoreHouseTabButtonUp", 1)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/CityTabButtonDown", 1)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/Tab03Down", 1)
   
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InStorehouse", 1)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InCity", 0)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InMulti", 0)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/DialogButtons/PlayerButtons/DestroyGoods", 1)

end



function GUI_Trade.CityTabButtonClicked()

    g_Trade.GoodType = 0
    g_Trade.GoodAmount = 0

    Sound.FXPlay2DSound("ui\\menu_click")

    XGUIEng.ShowAllSubWidgets("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons", 0)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/StoreHouseTabButtonDown", 1)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/CityTabButtonUp", 1)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InStorehouse", 0)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InCity", 1)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InMulti", 0)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/DialogButtons/PlayerButtons/DestroyGoods", 0)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/Tab03Down", 1)
    

end

function GUI_Trade.StorehouseTabButtonClicked()

    g_Trade.GoodType = 0
    g_Trade.GoodAmount = 0

    Sound.FXPlay2DSound("ui\\menu_click")

    XGUIEng.ShowAllSubWidgets("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons", 0)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/StoreHouseTabButtonUp", 1)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/CityTabButtonDown", 1)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InStorehouse", 1)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InCity", 0)
    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/InMulti", 0)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/DialogButtons/PlayerButtons/DestroyGoods", 1)

    XGUIEng.ShowWidget("/InGame/Root/Normal/AlignBottomRight/Selection/Storehouse/TabButtons/Tab03Down", 1)
    

end

function GUI_Trade.GetPlayerOfferCountOfGoodType(_PlayerID, _TargetPlayerID, _GoodType)

    local StorhouseID = Logic.GetStoreHouse(_TargetPlayerID)

    local TradersInTheActiveBuilding = Logic.GetNumberOfMerchants(StorhouseID)

    -- has storehouse of player a goodtrader?
    for i = 0, TradersInTheActiveBuilding - 1 do

        if Logic.IsGoodTrader(StorhouseID, i) then

            local Offers = {Logic.GetMerchantOfferIDs(StorhouseID, i,_PlayerID)}

            for j = 1, #Offers do

                local OfferID = Offers[j]

                local OfferGoodType, GoodAmount, OfferAmount, AmountPrices = Logic.GetGoodTraderOffer(StorhouseID,OfferID,_PlayerID)

                if OfferGoodType == _GoodType then
                    local OfferCount = Logic.GetOfferCount(StorhouseID,OfferID,_PlayerID)
                    return OfferCount
                end

            end
        end
    end

    return 0

end


